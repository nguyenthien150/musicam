package com.example.appnhac.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.appnhac.R;
import com.example.appnhac.SplashScreen.OnBoarding;

public class BeginningActivity extends AppCompatActivity {

    private static int SPLASH_SCREEN = 3000;
    private Animation animationTop;
    private Animation animationBottom;
    private ImageView image, image2;
    private SharedPreferences onBoardingScreen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beginning);
        animationTop = AnimationUtils.loadAnimation(this, R.anim.animation);
        image = findViewById(R.id.logo);
        image2 = findViewById(R.id.word);
        image.setAnimation(animationTop);
        animationBottom = AnimationUtils.loadAnimation(this, R.anim.animation2);
        image2.setAnimation(animationBottom);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                onBoardingScreen=getSharedPreferences("onBoardingScreen",MODE_PRIVATE);

                boolean isFirstTime=onBoardingScreen.getBoolean("firstTime",true);
                if (isFirstTime){
                    SharedPreferences.Editor editor = onBoardingScreen.edit();
                    editor.putBoolean("firstTime", false);
                    editor.apply();


                    Intent intent = new Intent(getApplicationContext(), OnBoarding.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_out_right, R.anim.slide_out_left);
                    finish();
                }
                else{
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        },SPLASH_SCREEN);

    }
}
