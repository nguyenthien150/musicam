package com.example.appnhac.SplashScreen;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.appnhac.Activity.MainActivity;
import com.example.appnhac.Adapter.SliderAdapter;
import com.example.appnhac.R;

public class OnBoarding extends AppCompatActivity {

    private ViewPager viewPager;
    private LinearLayout dots_layout;
    private SliderAdapter sliderAdapter; //Adapter
    private  TextView[] dots;
    private Button getStartedButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);
        viewPager = findViewById(R.id.slider);
        dots_layout = findViewById(R.id.dots);
        sliderAdapter = new SliderAdapter(this);
        viewPager.setAdapter(sliderAdapter);
        addDots(0);
        getStartedButton = findViewById(R.id.get_started);
        viewPager.addOnPageChangeListener(changeListener);
    }
    private void addDots(int position)
    {
        dots = new TextView[3];
        dots_layout.removeAllViews();
        for(int i = 0; i < dots.length; i++)
        {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226"));
            dots[i].setTextSize(35);
            dots_layout.addView(dots[i]);
        }
        if(dots.length > 0)
        {
            dots[position].setTextColor(getResources().getColor(R.color.orange));
        }
    }
    ViewPager.OnPageChangeListener changeListener = new ViewPager.OnPageChangeListener()
    {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {

        }

        @Override
        public void onPageSelected(int position)
        { addDots(position);
            if (position ==0)
            {getStartedButton.setVisibility(View.INVISIBLE);}
            else if(position==1){getStartedButton.setVisibility(View.INVISIBLE);}
            else if (position ==2){getStartedButton.setVisibility(View.VISIBLE);}


        }

        @Override
        public void onPageScrollStateChanged(int state)
        {
        }
    };
    public void bttn_started(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);

        finish();
    }
}