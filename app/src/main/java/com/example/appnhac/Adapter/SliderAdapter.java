package com.example.appnhac.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.appnhac.R;

public class SliderAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    public SliderAdapter(Context context) {
        this.context = context;
    }

    int image[] = {
            R.drawable.fox,
            R.drawable.catmusic,
            R.drawable.beeth,
    };

    int headings[] = {
            R.string.get_started,
            R.string.description_1,
            R.string.description_4
    };
    int description[] = {
            R.string.description_2,
            R.string.description_3,
            R.string.Beethoven
    };

    @Override
    public int getCount() {
        return headings.length ;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (ConstraintLayout) object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        layoutInflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);
        pl.droidsonroids.gif.GifImageView gifView = view.findViewById(R.id.gifImageView);
        //ImageView imageView  = view.findViewById(R.id.slider_image);
        TextView heading = view.findViewById(R.id.slider_heading);
        TextView desc = view.findViewById(R.id.slider_desc);

        gifView.setImageResource(image[position]);
        //imageView.setImageResource(image[position] );
        heading.setText(headings[position]);
        desc.setText(description[position]);

        container.addView(view);

        return view;
    }
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout)object);
    }
}
